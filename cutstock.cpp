#include <ilcplex/ilocplex.h>
#include <bits/stdc++.h>

ILOSTLBEGIN

#define EPS 1.0e-6
#define pb push_back

vector<vector<int> > patts;
vector<double>  boardlen,need;

void printVector(ofstream &os,vector<int> a,int fl = 0){
   os<<"[ ";
   for(int i =0;i<a.size();i++) {
      os<< a[i] << " ";
   }
   os<< "]";
   if(!fl) os<<endl;
}
void printVecdouble(ofstream &os,vector<double> a){
   os<<"[ ";
   for(int i =0;i<a.size();i++) {
      os<< a[i] << " ";
   }
   os<< "]"<<endl;
}


string cur;
int getno(){
   string s;
   while((cin>>s)){
      if(s[0]>='0'&&s[0]<='9'){
         cur = s;
         int x=0;
         
         for(int i=0;i<s.size();i++) 
            x=x*10+s[i]-'0';
         
         return x;
      
      }else continue;
   }
}

double getdouble(){
   string s;
   while((cin>>s)){
      if(s[0]>='0'&&s[0]<='9'||s[0]=='.'){
         cur=s;
         double x=0,p=1;
         int i;
         for(i=0;i<s.size()&&s[i]!='.';i++) 
            x=x*10+s[i]-'0';
         if(i<s.size()&&s[i]=='.'){
            i++;
            while(i<s.size()){
               p/=10;
               x+=p*(s[i]-'0');
               i++;
            }
         }
      //   printf("%lf\n",x);
         return x;
      
      }else continue;
   }  
}


int main(int argc, char **argv){

   IloEnv evk;
   
   IloInt  i, j;

   IloNum      rollWidth;
   IloNumArray amount(evk);
   IloNumArray size(evk);

   if ( argc > 1 )
      freopen(argv[1],"r",stdin);
   else
      freopen("input.txt","r",stdin);

   string roll; 
   double rolllen = getdouble();
   roll = cur;

   int mySize = getno();
   string amt,sz;
   amt="[",sz="[";
   for(int i=0;i<mySize;i++){
      double x = getdouble();
      boardlen.pb(x);

      if(i>0) amt=amt+",",sz=sz+",";
      amt=amt+cur;
      x=getno();  
      need.pb(x);
      sz=sz+cur;
   }
   amt=amt+"]"; sz=sz+"]";
   if(1){
      std::ofstream os("doit.txt");
      os<<roll<<endl<<amt<<endl<<sz;
      os.close();
   }
   ifstream in("doit.txt");
   in >> rollWidth;
   in >> size;
   in >> amount;


   IloModel cutOpt (evk);

   IloObjective   RollsUsed = IloAdd(cutOpt, IloMinimize(evk));
   IloRangeArray  Fill = IloAdd(cutOpt,
                                IloRangeArray(evk, amount, IloInfinity));
   IloNumVarArray Cut(evk);

   IloInt noItems = size.getSize();

   std:: ofstream olog("log.txt");
   olog<<"Initial Patterns added to the master problem"<<endl;
   for (j = 0; j < noItems; j++) {

      Cut.add(IloNumVar(RollsUsed(1) + Fill[j](int(rollWidth / size[j]))));
      vector<int> a;
      for(int k = 0;k<noItems;k++) a.pb(0);
      a[j]=rollWidth/size[j];
      patts.pb(a);
      olog<< (j+1) << ") ";
      printVector(olog,a);
   }
   olog <<endl;
   
   IloCplex cutSolver(cutOpt);

   cutSolver.setOut(evk.getNullStream());

   IloModel patGen (evk);

   IloObjective ReducedCost = IloAdd(patGen, IloMinimize(evk, 1));
   IloNumVarArray Use(evk, noItems, 0.0, IloInfinity, ILOINT);
   patGen.add(IloScalProd(size, Use) <= rollWidth);

   IloCplex patSolver(patGen);

   patSolver.setOut(evk.getNullStream());

   IloNumArray price(evk, noItems);
   IloNumArray newPatt(evk, noItems);



   for (int ct=1;;ct++) {
      
      olog << "Iteration " <<ct <<":"<<endl;
      cutSolver.solve();
      olog << "Objective value of the master-problem: " << cutSolver.getObjValue()  <<endl;
      olog << "Cut values for patterns: [ ";
      for (IloInt j = 0; j < Cut.getSize(); j++) {
         olog << cutSolver.getValue(Cut[j]) << " ";
      }
      olog << "]"<<endl;
    
      for (i = 0; i < noItems; i++) {
        price[i] = -cutSolver.getDual(Fill[i]);
      }
      ReducedCost.setLinearCoefs(Use, price);
    
      patSolver.solve();

      olog << "Objective value of the sub-problem: " << patSolver.getValue(ReducedCost) <<endl;
      if (patSolver.getValue(ReducedCost) <= -EPS) {
         
         vector<int> a;

         for (IloInt i = 0; i < Use.getSize(); i++)  {
            a.pb(patSolver.getValue(Use[i]));
         }
         patts.pb(a);
         olog << "New pattern generated: " <<endl;
         printVector(olog,a);
      }
      if (patSolver.getValue(ReducedCost) > -EPS) break;
    
      patSolver.getValues(newPatt, Use);
      Cut.add( IloNumVar(RollsUsed(1) + Fill(newPatt)) );
      
      olog <<endl;
   }



   olog << "\nOptimality attained for the master-problem.\n";
   cutOpt.add(IloConversion(evk, Cut, ILOINT));

   cutSolver.solve();
   std:: ofstream out("output.txt");
   int mytot = 0;
   for (IloInt j = 0; j < Cut.getSize(); j++) if(abs(cutSolver.getValue(Cut[j])+0.5)>=1){
      mytot += (int)(cutSolver.getValue(Cut[j])+0.5);
   }
   out<< "No. of stocks to be cut: " << mytot << endl;
   double ccut = rolllen *  mytot ,total = 0 ;
   for(int i = 0 ; i < boardlen.size(); i++){
      total+= boardlen[i]*need[i];
   }
   double Wasteper = (ccut - total )*100 / total;
   out << "Waste percentage: " << Wasteper <<endl<<endl ;
   out << "Order Lengths: ";
   printVecdouble(out,boardlen);

   out << "\nCutting Pattern         No. of times cut\n";
   for (IloInt j = 0; j < Cut.getSize(); j++) if(abs(cutSolver.getValue(Cut[j])+0.5)>=1){
      printVector(out,patts[j],1);
      out << "              " << (int)(cutSolver.getValue(Cut[j])+0.5) << endl;
   }

   evk.end();

   return 0;
}



