# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Problem statement- Woodco sells 110-ft, 81-ft, 64-ft, 51.75-ft, 41.25-ft and 21.50-ft pieces of lumber. Woodco’s customers demand 337 110-ft boards, 415 81-ft boards, 263 64-ft boards, 736 51.75-ft boards, 393 41.25-ft boards, and 140 21.50-ft boards. Woodco, who must meet its demands by cutting up 245-ft boards, wants to minimize the waste incurred. Formulate an LP to help Woodco accomplish its goal, and solve the LP by column generation.